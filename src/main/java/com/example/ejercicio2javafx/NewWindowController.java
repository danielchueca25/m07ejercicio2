package com.example.ejercicio2javafx;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.text.Text;

import java.net.URL;
import java.util.ResourceBundle;

public class NewWindowController implements Initializable {

    @FXML
    public Text nombre;
    @FXML
    public Text ciudad;
    @FXML
    public Text telefono;
    @FXML
    public Text juego;
    @FXML
    public Text generacion;



    @FXML
    protected void closeWindow(){
       HelloController.stage.close();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {}

    public void traspasarInformacion(String nombre, String ciudad, String telefono, String juego, String generacion){//recibes datos del otro
        set(nombre, this.nombre);
        set(ciudad, this.ciudad);
        set(telefono, this.telefono);
        set(juego, this.juego);
        set(generacion, this.generacion);
    }

    private boolean set(String texto, Text text){
        if(texto!=null){
            if(!texto.equalsIgnoreCase("")){
                text.setText(texto);
                return true;
            }
        }
        text.setText("Text Not Found");
        return false;
    }
}
