package com.example.ejercicio2javafx;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;

import java.io.IOException;

public class HelloController {
    public static  Stage stage=new Stage();

    public static void changeToScene2(ActionEvent cambio) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("newWindow.fxml"));
            Scene scene = new Scene(fxmlLoader.load(),400,300);

            NewWindowController consultaController=fxmlLoader.getController();
            consultaController.traspasarInformacion("",
                   "",
                    "",
                    "",
                  "");



            stage.setTitle("Consulta window");
            stage.setScene(scene);
            stage.show();


        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}